import { 
  connectRoutes
} from 'redux-first-router';
// import createHistory from 'history/createBrowserHistory';
// import { apiMiddleware } from 'redux-api-middleware';
import { redirect } from 'redux-first-router';
// import createSagaMiddleware from 'redux-saga';
import queryString from 'query-string';
import { find } from 'lodash';
// ROUTES
export const ROUTE_HOME = 'route/ROUTE_HOME';
export const ROUTE_ABOUT = 'route/ROUTE_ABOUT';
export const ROUTE_LOGIN = 'route/ROUTE_LOGIN';
export const ROUTE_PAYLOAD = 'route/ROUTE_PAYLOAD';
export const ROUTE_SIGNUP = 'route/ROUTE_SIGNUP';
// selector
export const routeType = state => state.location.type
//routes map
export const routesMap = {
  [ROUTE_HOME]: {
    path: "/",
    component: "Home",
  },
  [ROUTE_ABOUT]:
  {
    path: '/about',
    component: "Home",
  },
  [ROUTE_LOGIN]: {
    path: "/login",
    component: "Auth",
    requiresAuth: false
  },
  [ROUTE_SIGNUP]: {
    path: '/signup',
    component: 'Signup',
    requiresAuth: false
  },
  [ROUTE_PAYLOAD]: {
    path: "/payload/:advertiserId",
    modalOver: ROUTE_HOME,
    component: "Home",
    requiresAuth: true
  }
}

export const routeLogin = () => ({
  type: ROUTE_LOGIN
})

const {
  reducer,
  middleware,
  enhancer
} = connectRoutes(routesMap, {
  querySerializer: queryString,
  initialDispatch: false,
  onBeforeChange: (dispatch, getState, { action }) => {
    const routeDefinition = routesMap[action.type];

    if (routeDefinition && routeDefinition.redirects) {
      const matchedRedirect = find(
        routeDefinition.redirects,
        ({ test }) => !!test(getState, action)
      );

      matchedRedirect && dispatch(redirect(matchedRedirect.to));
    }
  }
})

export {
  reducer,
  middleware,
  enhancer
}


